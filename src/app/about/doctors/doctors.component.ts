import { card } from 'src/app/service/interfaces';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal!: ElementRef;

  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',

        heading: 'نقيب أطباء القاهرة',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'الأمين العام',
        content: 'د/ محمد احمد محمود',

      },

    ];
  }

  close(){
    this.closeAddExpenseModal.nativeElement.click();
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
