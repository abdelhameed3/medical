import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-laws',
  templateUrl: './laws.component.html',
  styleUrls: ['./laws.component.scss']
})
export class LawsComponent implements OnInit {

  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        heading: 'قانون النقابة',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'قانون النقابة',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'قانون النقابة',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'قانون النقابة',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'قانون النقابة',
      },
    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
