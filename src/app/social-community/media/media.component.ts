import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {


  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        colored: false,
        url: '/social/media/1',
        heading: 'الأخطاء الطبية ~ فيلم قصير',
        date:'12/08/2021',
        isMedia:true
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: false,
        url: '/social/media/1',
        heading: 'الأخطاء الطبية ~ فيلم قصير',
        date:'12/08/2021',
        isMedia:true
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: false,
        url: '/social/media/1',
        heading: 'الأخطاء الطبية ~ فيلم قصير',
        date:'12/08/2021',
        isMedia:true
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: false,
        url: '/social/media/1',
        heading: 'الأخطاء الطبية ~ فيلم قصير',
        date:'12/08/2021',
        isMedia:true
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: false,
        url: '/social/media/1',
        heading: 'الأخطاء الطبية ~ فيلم قصير',
        date:'12/08/2021',
        isMedia:true
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: false,
        url: '/social/media/1',
        heading: 'الأخطاء الطبية ~ فيلم قصير',
        date:'12/08/2021',
        isMedia:true
      },
    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
