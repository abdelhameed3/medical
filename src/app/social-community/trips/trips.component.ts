import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {

  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'

      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'السعر 400 جنيها للفرد البالغ <br>300 جنيها للطفل',
       url: '/social/trips/1',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },

    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
