import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-summer',
  templateUrl: './summer.component.html',
  styleUrls: ['./summer.component.scss']
})
export class SummerComponent implements OnInit {

  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [

      {
        img: 'https://via.placeholder.com/300',
        colored: true,
       url: '/social/summer/1',
        price: 'سعر الفرد يبدأ من 5000 جنيه',
        heading: 'مصيف دهب 6 أيام 5 ليالي',
        content: 'الرحلة الأولى : 20/5/2022 <br> الرحلة الثانية : 20/6/2022 <br> الرحلة الثالثة : 20/7/2022'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
       url: '/social/summer/1',
        price: 'سعر الفرد يبدأ من 5000 جنيه',
        heading: 'مصيف دهب 6 أيام 5 ليالي',
        content: 'الرحلة الأولى : 20/5/2022 <br> الرحلة الثانية : 20/6/2022 <br> الرحلة الثالثة : 20/7/2022'

      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
       url: '/social/summer/1',
        price: 'سعر الفرد يبدأ من 5000 جنيه',
        heading: 'مصيف دهب 6 أيام 5 ليالي',
        content: 'الرحلة الأولى : 20/5/2022 <br> الرحلة الثانية : 20/6/2022 <br> الرحلة الثالثة : 20/7/2022'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
       url: '/social/summer/1',
        price: 'سعر الفرد يبدأ من 5000 جنيه',
        heading: 'مصيف دهب 6 أيام 5 ليالي',
        content: 'الرحلة الأولى : 20/5/2022 <br> الرحلة الثانية : 20/6/2022 <br> الرحلة الثالثة : 20/7/2022'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
       url: '/social/summer/1',
        price: 'سعر الفرد يبدأ من 5000 جنيه',
        heading: 'مصيف دهب 6 أيام 5 ليالي',
        content: 'الرحلة الأولى : 20/5/2022 <br> الرحلة الثانية : 20/6/2022 <br> الرحلة الثالثة : 20/7/2022'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: 'سعر الفرد يبدأ من 5000 جنيه',
        heading: 'مصيف دهب 6 أيام 5 ليالي',
        content: 'الرحلة الأولى : 20/5/2022 <br> الرحلة الثانية : 20/6/2022 <br> الرحلة الثالثة : 20/7/2022',
       url: '/social/summer/1',
      },

    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
