import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
          date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',

      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
           date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',

          date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
          date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },{
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
          date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'لجنة الاسكان تعلن عن وحدات مصيفية بمدينة الغردقة',
          date: '2021-02-13',
colored: true,
url: '/social/projects/1',
      }
    ];
  }

  openPage(event:number){
    console.log('event',event);
  }
}
