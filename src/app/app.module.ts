import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CardComponent } from './shared/card/card.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ConfirmComponent } from './modal/confirm/confirm.component';
import { LoginComponent } from './modal/login/login.component';
import { ActiveAccountComponent } from './modal/active-account/active-account.component';
import { ProjectsComponent } from './social-community/projects/projects.component';
import { TripsComponent } from './social-community/trips/trips.component';
import { SummerComponent } from './social-community/summer/summer.component';
import { MediaComponent } from './social-community/media/media.component';
import { BannerComponent } from './shared/banner/banner.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { SponsorAdComponent } from './shared/sponsor-ad/sponsor-ad.component';
import { PaginationComponent } from './shared/pagination/pagination.component';
import { OffersComponent } from './offers/offers.component';
import { CoursesComponent } from './medical-science/courses/courses.component';
import { WorkshopsComponent } from './medical-science/workshops/workshops.component';
import { ConferenceComponent } from './medical-science/conference/conference.component';
import { DoctorsComponent } from './about/doctors/doctors.component';
import { LawsComponent } from './about/laws/laws.component';
import { DoctorServicesComponent } from './doctor-services/doctor-services.component';
import { DoctorInterestComponent } from './doctor-interest/doctor-interest.component';
import { MediaDetailsComponent } from './details/media-details/media-details.component';
import { DoctorDetailsComponent } from './modal/doctor-details/doctor-details.component';
import { WorkshopDetailsComponent } from './details/workshop-details/workshop-details.component';
import { CourseDetailsComponent } from './details/course-details/course-details.component';
import { ProjectsDetailsComponent } from './details/projects-details/projects-details.component';
import { SwiperModule } from 'swiper/angular';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SummerDetailsComponent } from './details/summer-details/summer-details.component';
import { TripDetailsComponent } from './details/trip-details/trip-details.component';
import { MedicalModalComponent } from './modal/medical-modal/medical-modal.component';

@NgModule({
  declarations: [
    AppComponent,

    NavbarComponent,
    FooterComponent,
    HomeComponent,
    CardComponent,
    ConfirmComponent,
    LoginComponent,
    ActiveAccountComponent,
    ProjectsComponent,
    TripsComponent,
    SummerComponent,
    MediaComponent,
    BannerComponent,
    BreadcrumbComponent,
    SponsorAdComponent,
    PaginationComponent,
    OffersComponent,
    CoursesComponent,
    WorkshopsComponent,
    ConferenceComponent,
    DoctorsComponent,
    LawsComponent,
    DoctorServicesComponent,
    DoctorInterestComponent,
    MediaDetailsComponent,
    DoctorDetailsComponent,
    WorkshopDetailsComponent,
    CourseDetailsComponent,
    ProjectsDetailsComponent,
    SummerDetailsComponent,
    TripDetailsComponent,
    MedicalModalComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CarouselModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    SwiperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
