import { TripDetailsComponent } from './details/trip-details/trip-details.component';
import { SummerDetailsComponent } from './details/summer-details/summer-details.component';
import { ProjectsDetailsComponent } from './details/projects-details/projects-details.component';
import { CourseDetailsComponent } from './details/course-details/course-details.component';
import { WorkshopDetailsComponent } from './details/workshop-details/workshop-details.component';
import { MediaDetailsComponent } from './details/media-details/media-details.component';
import { DoctorInterestComponent } from './doctor-interest/doctor-interest.component';
import { DoctorServicesComponent } from './doctor-services/doctor-services.component';
import { LawsComponent } from './about/laws/laws.component';
import { DoctorsComponent } from './about/doctors/doctors.component';
import { ConferenceComponent } from './medical-science/conference/conference.component';
import { WorkshopsComponent } from './medical-science/workshops/workshops.component';
import { CoursesComponent } from './medical-science/courses/courses.component';
import { OffersComponent } from './offers/offers.component';
import { ProjectsComponent } from './social-community/projects/projects.component';
import { SummerComponent } from './social-community/summer/summer.component';
import { TripsComponent } from './social-community/trips/trips.component';
import { ActiveAccountComponent } from './modal/active-account/active-account.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MediaComponent } from './social-community/media/media.component';

const routes: Routes = [
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'account-active',
    component:ActiveAccountComponent
  },
  {
    path:'social/trips',
    component:TripsComponent
  },
  {
    path:'social/trips/:id',
    component:TripDetailsComponent
  },
  {
    path:'social/summer',
    component:SummerComponent
  },
  {
    path:'social/summer/:id',
    component:SummerDetailsComponent
  },
  {
    path:'social/projects',
    component:ProjectsComponent
  },
  {
    path:'social/projects/:id',
    component:ProjectsDetailsComponent
  },
  {
    path:'social/media',
    component:MediaComponent
  },
  {
    path:'medical-science/courses',
    component:CoursesComponent
  },
  {
    path:'medical-science/courses/:id',
    component:CourseDetailsComponent
  },
  {
    path:'medical-science/workshops',
    component:WorkshopsComponent
  },
  {
    path:'medical-science/workshops/:id',
    component:WorkshopDetailsComponent
  },
  {
    path:'medical-science/conference',
    component:ConferenceComponent
  },
  {
    path:'about/doctors',
    component:DoctorsComponent
  },
  {
    path:'about/laws',
    component:LawsComponent
  },
  {
    path:'services',
    component:DoctorServicesComponent
  },
  {
    path:'medical',
    component:DoctorInterestComponent
  },
  {
    path:'social/media/:id',
    component:MediaDetailsComponent
  },
  {
    path:'offers',
    component:OffersComponent
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
