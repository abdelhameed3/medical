import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorInterestComponent } from './doctor-interest.component';

describe('DoctorInterestComponent', () => {
  let component: DoctorInterestComponent;
  let fixture: ComponentFixture<DoctorInterestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorInterestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
