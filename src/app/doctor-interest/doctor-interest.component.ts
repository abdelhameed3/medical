import { card } from 'src/app/service/interfaces';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-doctor-interest',
  templateUrl: './doctor-interest.component.html',
  styleUrls: ['./doctor-interest.component.scss']
})
export class DoctorInterestComponent implements OnInit {
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal!: ElementRef;

  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'

      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },
      {
        img: 'https://via.placeholder.com/300',
        colored: true,
        price: '2500 جنية / شهريا',
        url: 'https://via.placeholder.com/300',
        heading: 'عيادة للشراكة',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        date: 'من 20 أكتوبر إلى 20 نوفمبر'
      },

    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
  close(){
    this.closeAddExpenseModal.nativeElement.click();
  }
}
