import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {


  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',


      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'خصم 25% على الفاتورة لبوفيه العشاء المفتوحفي فندق الكونكورد ماعدا يومي الخميس و الجمعة',
        content: 'يرجى إظهار كارنيه النقابة للتمتع بالخصم',

      },

    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
