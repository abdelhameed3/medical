export interface card {
  img?:string,
  heading?:string,
  content?:string,
  url?:string,
  isCenter?:boolean,
  colored?:boolean,
  btnColored?:boolean,
  btnText?:string,
  price?:string,
  date?:string,
  isMedia?:boolean
}
