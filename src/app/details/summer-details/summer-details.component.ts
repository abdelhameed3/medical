import { Component, OnInit } from '@angular/core';
import { SwiperComponent } from "swiper/angular";

// import Swiper core and required modules
import SwiperCore, { FreeMode, Navigation, Thumbs } from "swiper";
@Component({
  selector: 'app-summer-details',
  templateUrl: './summer-details.component.html',
  styleUrls: ['./summer-details.component.scss']
})
export class SummerDetailsComponent implements OnInit {
  thumbsSwiper: any;

  constructor() { }

  ngOnInit(): void {
  }

}
