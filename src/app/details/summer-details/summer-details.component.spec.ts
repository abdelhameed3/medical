import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummerDetailsComponent } from './summer-details.component';

describe('SummerDetailsComponent', () => {
  let component: SummerDetailsComponent;
  let fixture: ComponentFixture<SummerDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummerDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
