import { Component, OnInit } from '@angular/core';
import { SwiperComponent } from "swiper/angular";

// import Swiper core and required modules
import SwiperCore, { FreeMode, Navigation, Thumbs } from "swiper";

// install Swiper modules
SwiperCore.use([FreeMode, Navigation, Thumbs]);
@Component({
  selector: 'app-medical-modal',
  templateUrl: './medical-modal.component.html',
  styleUrls: ['./medical-modal.component.scss']
})
export class MedicalModalComponent implements OnInit {
  thumbsSwiper: any;

  constructor() { }

  ngOnInit(): void {
  }

}
