import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SponsorAdComponent } from './sponsor-ad.component';

describe('SponsorAdComponent', () => {
  let component: SponsorAdComponent;
  let fixture: ComponentFixture<SponsorAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SponsorAdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SponsorAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
