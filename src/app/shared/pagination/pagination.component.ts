import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
// Numbe of data at one page
@Input() pageSize:number = 0;

// CurrentPage
 pageIndex:number = 0;

// Number of Total page
@Input() pages:number = 0;

pagesArray=[]

// EventEmitter for PageIndex
@Output() PageIndexEvent:EventEmitter<number> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.pagesArray.length = this.pages;

  }
  openPage(pageIndex:number){
    this.pageIndex = pageIndex;
    this.PageIndexEvent.emit(pageIndex);
  }
}
