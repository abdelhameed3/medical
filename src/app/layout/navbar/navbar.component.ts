import { HelperService } from './../../service/helper.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal!: ElementRef;

  lang = '';
  currentLang!:string
  constructor( public translate: TranslateService,private helper:HelperService) {}


  ngOnInit(): void {
    this.helper.currentLang.subscribe((value)=>{
      this.currentLang = value;
    })
  }
  changeLanguage() {
    const lang = this.currentLang == 'en' ? 'ar' : 'en'
    localStorage.setItem('lang', lang);
    this.langChanged(lang);
    this.translate.use(lang);
    this.lang = lang;
    this.helper.currentLang.next(lang);
    // location.reload();
  }
  langChanged(lang:string) {
    if (lang === 'ar') {
      // add bootstrap ar
      this.generateLinkElement({
        dir: 'rtl',
        lang: 'ar',
      });
      document.querySelector('#bootstrap-en')?.setAttribute('href','assets/vendor/bootstrap/bootstrap.rtl.min.css');
    }
    else {
      // en
      this.generateLinkElement({
        dir: 'ltr',
        lang: 'en',
      });
      document.querySelector('#bootstrap-en')?.setAttribute('href','assets/vendor/bootstrap/bootstrap.min.css');
    }
  }
  close(){
    this.closeAddExpenseModal.nativeElement.click();
  }
  generateLinkElement(props:any) {
    const htmlEl = document.getElementsByTagName('html')[0];
    htmlEl.setAttribute('dir', props.dir);
    htmlEl.setAttribute('lang', props.lang);
  }


}
