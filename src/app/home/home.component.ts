import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { SwiperOptions } from 'swiper';
import {NgwWowService} from 'ngx-wow';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cards: card[] = [];
  interests: card[] = [];
  sponsors: string[] = [];
  customOptions!: OwlOptions;
  sponserOptions!: OwlOptions;

  public config: SwiperOptions = {
    a11y: { enabled: true },
    direction: 'horizontal',
    lazy: true,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    pagination: false,
    centeredSlides: false,
    loop: true,

    slidesPerView: 5,
    breakpoints: {
      0: {
        slidesPerView: 1,
        spaceBetween: 15,
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 15,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 30,
      },
      1400: {
        slidesPerView: 5,
        spaceBetween: 30,
      },
    },
  };

  constructor(  private wowService: NgwWowService,) { }

  ngOnInit(): void {
    this.wowService.init();

    this.customOptions = {
      loop: true,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      margin: 30,
      rtl: true,
      autoHeight: false,
      center: false,
      navSpeed: 700,
      navText: ['', ''],
      items: 5,
      responsive: {
        0: {
          items: 1
        },
        400: {
          items: 2
        },
        740: {
          items: 3
        },
        940: {
          items: 4
        },
        1200: {
          items: 5
        }
      },
      nav: false
    };
    this.sponserOptions = {
      loop: true,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      margin: 30,
      rtl: true,
      autoHeight: false,
      center: false,
      navSpeed: 700,
      navText: ['', ''],
      items: 11,
      responsive: {
        0: {
          items: 3
        },
        400: {
          items: 4
        },
        740: {
          items: 6
        },
        940: {
          items: 10
        },
        1200: {
          items: 11
        }
      },
      nav: false
    };
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        url: 'https://via.placeholder.com/300',
        isCenter: true,
        colored: true,
        btnColored: true,
        btnText: 'إعراف المزيد'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        url: 'https://via.placeholder.com/300',
        isCenter: true,
        colored: true,
        btnColored: true,
        btnText: 'إعراف المزيد'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        url: 'https://via.placeholder.com/300',
        isCenter: true,
        colored: true,
        btnColored: true,
        btnText: 'إعراف المزيد'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        url: 'https://via.placeholder.com/300',
        isCenter: true,
        colored: true,
        btnColored: true,
        btnText: 'إعراف المزيد'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'رحلة اليوم الواحد إلى خان الخليلي',
        content: 'الرحلة تبدأ من موقف السوبر جت في محطة ... ومدتها اثني عشر ساعة وتشمل الإنتقالات ةالخارجية و الداخلية, وجبة الغداء و العشاء.',
        url: 'https://via.placeholder.com/300',
        isCenter: true,
        colored: true,
        btnColored: true,
        btnText: 'إعراف المزيد'

      }
    ];
    this.interests = [
      {
        img: 'https://via.placeholder.com/300',
        heading: 'عيادة للإيجار',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        isCenter: true,
        url: 'https://via.placeholder.com/300',
        btnColored: false,
        btnText: 'المزيد عن الخدمات'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'عيادة للإيجار',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        isCenter: true,
        url: 'https://via.placeholder.com/300',
        btnColored: false,
        btnText: 'المزيد عن الخدمات'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'عيادة للإيجار',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        isCenter: true,
        url: 'https://via.placeholder.com/300',
        btnColored: false,
        btnText: 'المزيد عن الخدمات'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'عيادة للإيجار',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        isCenter: true,
        url: 'https://via.placeholder.com/300',
        btnColored: false,
        btnText: 'المزيد عن الخدمات'
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'عيادة للإيجار',
        content: 'مساحة متوسطة,تشطيب سوبر لوكس موقع محوري على ناصية شارع',
        isCenter: true,
        url: 'https://via.placeholder.com/300',
        btnColored: false,
        btnText: 'المزيد عن الخدمات'
      }
    ];
    this.sponsors = [
      "assets/images/sponsors/1.png",
      "assets/images/sponsors/2.png",
      "assets/images/sponsors/3.png",
      "assets/images/sponsors/4.png",
      "assets/images/sponsors/5.png",
      "assets/images/sponsors/1.png",
      "assets/images/sponsors/2.png",
      "assets/images/sponsors/3.png",
      "assets/images/sponsors/4.png",
      "assets/images/sponsors/1.png",
      "assets/images/sponsors/2.png",
      "assets/images/sponsors/3.png",
      "assets/images/sponsors/4.png",
      "assets/images/sponsors/5.png"
    ];
  }

}
