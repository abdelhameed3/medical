import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.scss']
})
export class ConferenceComponent implements OnInit {


  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',


      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },
      {
        img: 'https://via.placeholder.com/300',

        heading: 'ACC Middle East 2021 Conference مؤتمر ACC الشرق الأوسط 2021',
        content: 'للتفاصيل قم بزيارة الموقع <br> <a href="https://www.acc-middleeast.com/" target="_blank" class="url-link">www.acc-middleeast.com</a>',

      },

    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
