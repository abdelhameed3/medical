import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
  styleUrls: ['./workshops.component.scss']
})
export class WorkshopsComponent implements OnInit {


  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',

      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'ورشة عمل للأطباء بمشاركة الجمعية البريطانية المنظار عنق الرحم',
        content: '12/08/2022',
      },
    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
