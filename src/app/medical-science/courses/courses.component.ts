import { card } from 'src/app/service/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {


  cards: card[] = [];
  constructor() { }

  ngOnInit(): void {
    this.cards = [
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',

      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
      {
        img: 'https://via.placeholder.com/300',
        heading: 'المهارات العلمية الحديثة في ادارة العيادات المدرسية الذكية',
        content: '12/08/2022',
      },
    ];
  }

  openPage(event: number) {
    console.log('event', event);
  }
}
